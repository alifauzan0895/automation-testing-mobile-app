*** Settings ***
Library     AppiumLibrary

*** Keywords ***
Open mulai
    Open Application    http://127.0.0.1:4723/wd/hub
    ...    platformName=android
    ...    appium:platformVersion=11
    ...    appium:deviceName=RR8N302RHWH
    ...    appium:newCommandTimeout=3600
    ...    appium:nativeWebScreenshot=true
    ...    appium:automationName=automationTest
    ...    appium:ensureWebviewsHavePages=true
    ...    appium:appActivity=id.co.bankraya.apps
    ...    appium:appPackage=id.co.bankraya.apps.MainActivity
    ...    appium:connectHardwareKeyboard=true

*** Test Cases ***

Menu Topup e-Wallet Case
    Open mulai
    Click Element At Coordinates    coordinate_X=663    coordinate_Y=1182
    Wait Until Page Does Not Contain Element   xpath=//android.view.View[@content-desc="Mohon tunggu, ya.."]   timeout=20
    Click Element   xpath=//android.widget.Button[@content-desc="+ Topup Baru"]
    Click Element   xpath=//android.widget.ImageView[@content-desc="Pilih e-wallet"]
    Wait Until Page Does Not Contain Element   xpath=//android.view.View[@content-desc="Mohon tunggu, ya.."]   timeout=20
    Click Element   xpath=//android.view.View[@content-desc="Gopay"]
    Click Element   xpath=//android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText
    Input Text    xpath=//android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText    YourPhoneNumber
    Click Element   xpath=//android.view.View[@content-desc="Cek"]
    Wait Until Page Does Not Contain Element   xpath=//android.view.View[@content-desc="Mohon tunggu, ya.."]   timeout=20
    Click Element   xpath=//android.widget.Button[@content-desc="Lanjutkan"]
    Click Element   xpath=//android.widget.Button[@content-desc="1"]
    Click Element   xpath=//android.widget.Button[@content-desc="0"]
    Click Element   xpath=//android.widget.Button[@content-desc="000"]
    Click Element   xpath=//android.widget.Button[@content-desc="Lanjutkan"]
    Click Element   xpath=//android.widget.Button[@content-desc="Topup e-Wallet"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View/android.view.View/android.view.View[1]/android.widget.ImageView[2]
    Wait Until Page Does Not Contain Element   xpath=//android.view.View[@content-desc="Mohon tunggu, ya.."]   timeout=20
    Click Element   xpath=//android.widget.RelativeLayout/com.android.internal.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.ImageView
    Wait Until Element Is Visible   xpath=//android.widget.ImageButton[@content-desc="Kembali ke atas"]
    Click Element   xpath=//android.widget.ImageButton[@content-desc="Kembali ke atas"]
    Set Appium Timeout    5
    Click Element At Coordinates    coordinate_X=986    coordinate_Y=245
    
    

    