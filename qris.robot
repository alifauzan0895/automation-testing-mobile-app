*** Settings ***
Library     AppiumLibrary

*** Keywords ***
Open mulai
    Open Application    http://127.0.0.1:4723/wd/hub
    ...    platformName=android
    ...    appium:platformVersion=11
    ...    appium:deviceName=RR8N302RHWH
    ...    appium:newCommandTimeout=3600
    ...    appium:nativeWebScreenshot=true
    ...    appium:automationName=automationTest
    ...    appium:ensureWebviewsHavePages=true
    ...    appium:appActivity=id.co.bankraya.apps
    ...    appium:appPackage=id.co.bankraya.apps.MainActivity
    ...    appium:connectHardwareKeyboard=true

*** Test Cases ***

Menu QRIS Case
    Open mulai
    Click Element   xpath=//android.view.View[3]/android.widget.ImageView[1]
    Wait Until Page Does Not Contain Element   xpath=//android.view.View[1]/android.widget.Button   timeout=20
    Wait Until Page Does Not Contain Element   xpath=//android.view.View[@content-desc="Mohon tunggu, ya.."]   timeout=20
    Wait Until Element Is Visible   xpath=//android.view.View/android.view.View/android.view.View[2]/android.view.View/android.widget.EditText
    Click Element   xpath=//android.view.View/android.view.View/android.view.View[2]/android.view.View/android.widget.EditText
    Input Text   xpath=//android.view.View/android.view.View/android.view.View[2]/android.view.View/android.widget.EditText     100
    Click Element At Coordinates    coordinate_X=933    coordinate_Y=1702  
    Click Element   xpath=//android.widget.Button[@content-desc="Lanjutkan"]
    Click Element   xpath=//android.widget.Button[@content-desc="Bayar"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    

    