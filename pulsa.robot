*** Settings ***
Library     AppiumLibrary

*** Keywords ***
Open mulai
    Open Application    http://127.0.0.1:4723/wd/hub
    ...    platformName=android
    ...    appium:platformVersion=11
    ...    appium:deviceName=RR8N302RHWH
    ...    appium:newCommandTimeout=3600
    ...    appium:nativeWebScreenshot=true
    ...    appium:automationName=automationTest
    ...    appium:ensureWebviewsHavePages=true
    ...    appium:appActivity=id.co.bankraya.apps
    ...    appium:appPackage=id.co.bankraya.apps.MainActivity
    ...    appium:connectHardwareKeyboard=true

*** Test Cases ***
# First Test Case
    # # Transfer ke Rekening
    # Open mulai
    # Click Element    xpath=//android.widget.ImageView[@content-desc="Transfer"]
    # Set Appium Timeout    5
    # Click Element    xpath=//android.widget.Button[@content-desc="+ Transfer Baru"]
    # Set Appium Timeout    5
    # Click Element    xpath=//android.widget.ImageView[@content-desc="Pilih bank penerima"]
    # Set Appium Timeout    5
    # Click Element    class=android.widget.ImageView
    # Input Text    class=android.widget.ImageView    jago
    # Click Element    xpath=//android.view.View[@content-desc="BANK JAGO, TBK"]
    
# Login Test Case
    # Masuk Ke Halaman Login Password
    # # Transfer ke Rekening
    # Open mulai
    # Click Element    xpath=//android.widget.Button[@content-desc="Mulai Sekarang"]
    # Set Appium Timeout    5
    # Click Element    xpath=//android.view.View/android.widget.EditText
    # Input Text    xpath=//android.view.View/android.widget.EditText    YourPhoneNumber
    # Click Element    xpath=//android.widget.Button[@content-desc="Lanjut"]
    # Wait Until Element Is Visible    xpath=//android.view.View/android.widget.EditText
    # Click Element    xpath=//android.view.View/android.widget.EditText
    # Input Text    xpath=//android.view.View/android.widget.EditText    YourPassword
    # Click Element    xpath=//android.widget.Button[@content-desc="Masuk"]
    # Wait Until Element Is Visible   xpath=//android.view.View[@content-desc="Masuk menggunakan fingerprint / Face ID"]
    # Click Element  xpath=//android.widget.Button[@content-desc="Nanti Saja"]

# Menu Transfer Test Case
#     # Transfer ke Rekening
#     Open mulai
#     Wait Until Element Is Visible   xpath=//android.widget.ImageView[@content-desc="Transfer"]
#     Click Element  xpath=//android.widget.ImageView[@content-desc="Transfer"]
#     Wait Until Element Is Visible  xpath=//android.widget.Button[@content-desc="+ Transfer Baru"]
#     Click Element  xpath=//android.widget.Button[@content-desc="+ Transfer Baru"]
#     Click Element  xpath=//android.widget.ImageView[@content-desc="Pilih bank penerima"]
#     Wait Until Element Is Visible   xpath=//android.view.View/android.view.View/android.widget.ImageView
#     Click Element    xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.widget.ImageView[2]
#     Input Text    xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.widget.ImageView[2]      asia
#     Click Element    xpath=//android.view.View[@content-desc="BANK CENTRAL ASIA Tbk."]
#     Wait Until Element Is Visible   xpath=//android.view.View[@content-desc="Transfer"]
#     Click Element    xpath=///hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText
#     Input Text  xpath=///hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText       0881289520


# Menu Topup EWallet Case
    # Open mulai
    # Click Element   xpath=//android.widget.ImageView[@content-desc="Topup e-Wallet"]


Menu Pulsa Case
    Open mulai
    Click Element   xpath=//android.widget.ImageView[@content-desc="Pulsa"]
    Wait Until Page Does Not Contain Element   xpath=//android.view.View[@content-desc="Mohon tunggu, ya.."]   timeout=20
    Click Element At Coordinates    coordinate_X=300    coordinate_Y=749
    Click Element   xpath=//android.widget.Button[@content-desc="Lanjutkan"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View[@content-desc="0"]
    Click Element   xpath=//android.view.View/android.view.View/android.view.View[1]/android.widget.ImageView[2]
    Wait Until Page Does Not Contain Element   xpath=//android.view.View[@content-desc="Mohon tunggu, ya.."]   timeout=20
    Click Element   xpath=//android.widget.RelativeLayout/com.android.internal.widget.RecyclerView/android.widget.LinearLayout[4]/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.ImageView
    Wait Until Element Is Visible   xpath=//android.widget.ImageButton[@content-desc="Kembali ke atas"]
    Click Element   xpath=//android.widget.ImageButton[@content-desc="Kembali ke atas"]
    Set Appium Timeout    5
    Click Element At Coordinates    coordinate_X=986    coordinate_Y=245

    